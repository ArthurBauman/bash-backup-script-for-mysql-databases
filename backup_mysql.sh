#!/bin/bash

################################################################################
# Perform compressed backups of MySQL database and remove old backups
# Website:
#   https://bitbucket.org/ArthurBauman/bash-backup-script-for-mysql-databases/overview
# Arguments:
#   None
# Returns:
#   None
################################################################################
# Copyright (c) 2018 Arthur Bauman
# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the "Software"), to deal 
# in the Software without restriction, including without limitation the rights 
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
################################################################################

. $(dirname $0)/backup_mysql.cfg

function dump_compress_database () {
  local -r period=$1
  set +e
  error_message=$( mkdir -p "${BACKUP_DIR}"/"${period}" 2>&1 )
  error $? "${error_message}" 

  error_message=$( { mysqldump --opt -u root --password="${PASS}" --all-databases \
    > "${BACKUP_DIR}"/"${period}"/all_databases_"${date}".sql; } 2>&1 )
  error $? "${error_message}"

  error_message=$( gzip -f "${BACKUP_DIR}"/"${period}"/all_databases_"${date}".sql 2>&1 )
  error $? "${error_message}"

  for database in "${DATABASES[@]}"
  do
    error_message=$( { mysqldump --opt -u root --password="${PASS}" "$database" \
      > "${BACKUP_DIR}"/"${period}"/"${database}"_"${date}".sql; } 2>&1 )
    error $? "${error_message}"

    error_message=$( gzip -f "${BACKUP_DIR}"/"${period}"/"${database}"_"${date}".sql 2>&1 )
    error $? "${error_message}"
  done
  set -e
}

function remove_old_backup () {
  set +e
  #Remove old  backups. Keep minimum amount of backups.
  local -r period=$1
  local -r minimum_amount=$2
  local -r age=$3

  find_output=$( find "${BACKUP_DIR}"/"${period}" -type f  2>&1 )
  error $? "${find_output}"
  wc_output=$(wc -w <<<"${find_output}" 2>&1 )
  error $? "${wc_output}"

  if [ "${wc_output}" -gt "${minimum_amount}" ] ; then
    error_message=$(find ${BACKUP_DIR}/${period}/ -mtime +"${age}" -type f -delete  2>&1)
    error $? "${error_message}"
  fi
  set -e
}

function error () {
  if [ $1 -ne 0 ]; then
    echo  -e "$(date +"%Y-%m-%d %H:%M:%S")" FATAL ERROR "$0" Line: "$((${BASH_LINENO}-1))" $2  >> "${LOG_FILE}"
    exit 1
  fi
}

declare -r date=$(date +"%Y-%m-%d")

#Daily backup
dump_compress_database daily
remove_old_backup daily 7 7

#Weekly backup
declare -r day=$(date +%a)
if [ "${day}" == 'Sun' ] ; then
  dump_compress_database weekly
  remove_old_backup weekly 4 30
fi

#Monthly backup
today=$(/bin/date +%d)
tomorrow=$(/bin/date +%d -d "1 day")
if [ ${tomorrow} -lt ${today} ] ; then
  dump_compress_database monthly
  remove_old_backup monthly 12 365
fi

#Yearly backup
today=$(/bin/date +%Y)
tomorrow=$(/bin/date +%Y -d "1 day")
if [ ${tomorrow} -gt ${today} ] ; then
  dump_compress_database yearly
fi