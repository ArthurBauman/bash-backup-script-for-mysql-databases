#Bash script performing daily, weekly, monthly and yearly backups of MySQL databases.  

It creates backups of all databases and it creates backups of single databases specified in the config file.  
Backups are compressed.  
Script creates:

* 7 Daily backups
* 4 Weekly backups
* 12 Monthly backups
* Infinite yearly backups  

Redundant backups are automatically removed.

### Prerequisites

You need a Linux machine with: 

* MySQl database(s)
* Bash shell (standard on most Linux distributions).
* mysqldump program (mostly part of mysql-client).
* wc program (word count).
* Cron

### Installing ###

Download the following files to /usr/local/sbin/backup_mysql/:

* backup_mysql.sh
* backup_mysql_example.cfg  

Set owner and permissions:
```
sudo chown root backup_mysql.sh backup_mysql_example.cfg
```
```
sudo chmod 700 backup_mysql.sh backup_mysql_example.cfg
```
Edit backup_mysql_example.cfg It's self explanatory. 

Rename backup_mysql_example.cfg:  
```
sudo mv backup_mysql_example.cfg backup_mysql.cfg
```
Create a file  /etc/cron.daily/backup_mysql with this content:
```
#!/bin/bash
/usr/local/sbin/backup_mysql/backup_mysql.sh  >> /var/log/cron_daily_backup_mysql.log 2>&1
```

### Restoring database(s)
Uncompress desired database backup and restore.  

Example restoring a single database:
```
gunzip database_name_2018-01-23.sql.gz
mysql -u root -p database_name < database_name_2018-01-23.sql
```
Example restoring all databases:
```
gunzip all_databases_2018-01-23.sql.gz
mysql -u root -p < all_databases_2018-01-23.sql
```

### Acknowledgments ###

* Periodically check backups, /var/log/backup_mysql.log and /var/log/cron_daily_backup_mysql.log.
* Script does not backup stored procedures.

### License ###

 Copyright (c) 2018 Arthur Bauman

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
### Authors ###
Arthur Bauman
https://bauman.nu


